from Gaudi.Configuration import *
from Configurables import Kepler

# Set the number of events to run over 
Kepler().EvtMax = 10000 

# Set the input data
#gtkpath = "/afs/cern.ch/user/h/hschindl/keplervpx/TDCPixDecoder/data/"
gtkpath = "/afs/cern.ch/user/m/mperrint/public/TDCPixDecoder/data/"
inputFiles = [gtkpath + "s1-4_plane_0_0000024431.00000.bpkt32"]
tpx3path = "eos/lhcb/testbeam/velo/timepix3/na62/RawData/"
inputFiles += [tpx3path + "Run24431/"]

Kepler().InputFiles = inputFiles

Kepler().AlignmentFile = "Alignment_GTK.dat" 

def standalone():
  from Configurables import TbTdcPixDecoder
  TbTdcPixDecoder().Standalone = True
  TbTdcPixDecoder().Monitoring = True
  from Configurables import TbClustering
  GaudiSequencer("Telescope").Members = [TbTdcPixDecoder(), TbClustering()]
  from Configurables import TbHitMonitor
  from Configurables import TbClusterPlots
  GaudiSequencer("Monitoring").Members = [TbHitMonitor(), TbClusterPlots()]
appendPostConfigAction(standalone)
