import os
from Gaudi.Configuration import *
from Configurables import Kepler

# Set the number of events to run over. 
#Kepler().EvtMax = -1
Kepler().EvtMax = 200 

run = 24471
# Set the input data.
inputFiles = []
path = "eos/lhcb/wg/testbeam/velo/timepix3/na62/RawData/Run" + repr(run)
inputFiles = []
for dev in range(8):
  inputFiles += [path + "/Dev" + repr(dev) + "/"]
for gtk in range(3):
  gtkpath = "/" + path + "/TDCPix_Plane_" + repr(gtk) + "/"
  gtkfiles = os.listdir(gtkpath) 
  for f in gtkfiles:
    inputFiles += [gtkpath + f]
Kepler().InputFiles = inputFiles

# Set the alignment file.
Kepler().AlignmentFile = "Alignment_GTK.dat" 

# Timing offsets
Kepler().PixelConfigFile = ["TimingConfig.dat"]
# Masked pixels
Kepler().PixelConfigFile += ["PixelMask.dat"]

from Configurables import TbEventBuilder
TbEventBuilder().MaskedPlanes = [8, 9, 10]
TbEventBuilder().MinPlanesWithHits = 5

from Configurables import TbClusterPlots
TbClusterPlots().FillComparisonPlots = True
TbClusterPlots().ReferencePlane = 7
TbClusterPlots().ParametersDifferenceXY = ("", -5., 5., 200) 

from Configurables import TbSimpleTracking
TbSimpleTracking().MaskedPlanes = [8, 9, 10]

from Configurables import TbMillepede
#Kepler().addAlignment(TbMillepede(MaskedPlanes = [8, 9, 10], DOFs=[True, True, False, True, True, True], MaxChi2 = 5., ResCutInit=1.0, ResCut=0.2))
from Configurables import TbAlignmentMinuit2
Kepler().addAlignment(TbAlignmentMinuit2(DeviceToAlign=10, DOFs=[True, True, False, True, True, True], XWindow=1., TimeWindow=10.))
from Configurables import TbAlignment
TbAlignment().NTracks = 100000

# Hack to insert TbTdcPixDecoder in the reconstruction sequence.
def patch():
  from Configurables import MessageSvc
  #MessageSvc().setDebug = ["TbTdcPixDecoder2"]
  from Configurables import TbTdcPixDecoder, TbClustering
  decoder0 = TbTdcPixDecoder("TbTdcPixDecoder0")
  decoder0.Standalone = False
  decoder0.PrintFreq = 10000
  decoder0.DeviceName = "GTK_s1-4"
  decoder1 = TbTdcPixDecoder("TbTdcPixDecoder1")
  decoder1.Standalone = False
  decoder1.PrintFreq = 10000
  decoder1.DeviceName = "GTK_s4-2"
  decoder2 = TbTdcPixDecoder("TbTdcPixDecoder2")
  decoder2.Standalone = False
  decoder2.PrintFreq = 10000
  decoder2.DeviceName = "GTK_s9-4"
  decoder2.SkipFrames = 96
  from Configurables import TbClustering
  GaudiSequencer("Telescope").Members = [TbEventBuilder()]
  GaudiSequencer("Telescope").Members += [decoder0, decoder1, decoder2, TbClustering()]
  from Configurables import TbClusterAssociator
  TbClusterAssociator().DUTs = [8, 9, 10]
  TbClusterAssociator().XWindow = 1.
  TbClusterAssociator().TimeWindow = 10.
  GaudiSequencer("Telescope").Members += [TbSimpleTracking(), TbClusterAssociator()]
  #GaudiSequencer("Telescope").Members += [TbAlignment()]
  from Configurables import TbDUTMonitor
  TbDUTMonitor().DUTs = [8, 9, 10]
  TbDUTMonitor().ParametersResidualsXY = ("", -0.5, 0.5, 200) 
  #TbDUTMonitor().ParametersResidualsXY = ("", -20., 20., 500) 
appendPostConfigAction(patch)

