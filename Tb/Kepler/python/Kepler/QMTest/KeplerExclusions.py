from GaudiTest import LineSkipper, RegexpReplacer
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

base_preprocessor = (
    # Prevent the standard preprocessor to "mask pointers"
    RegexpReplacer(r"0x([0-9a-fA-F]{4,16})", r"0X\1") + LHCbPreprocessor +
    RegexpReplacer(r"0X([0-9a-fA-F]{4,16})", r"0x\1"))

preprocessor = (base_preprocessor + LineSkipper(["TbEventBuilder...."]) +
                LineSkipper(["VolFillTime"]))
