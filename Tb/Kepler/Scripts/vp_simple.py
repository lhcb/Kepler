from Gaudi.Configuration import *
from Configurables import Kepler

# Kepler().AlignmentFile = "/afs/cern.ch/user/e/edallocc/cmtuser/kepler_vpx/utils/Alignment.dat"
#Kepler().PixelConfigFile = ["/afs/cern.ch/user/e/edallocc/cmtuser/kepler_vpx/KEPLER/PixelMask_222_test.dat","/afs/cern.ch/user/e/edallocc/cmtuser/kepler_vpx/KEPLER/PixelMask_222_test2.dat"]
#Kepler().PixelConfigFile = ["/afs/cern.ch/user/e/edallocc/cmtuser/kepler_vpx/KEPLER/PixelMask_222_test.dat"]

# Set the configuration of the individual algorithms, e. g.
from Configurables import TbEventBuilder
TbEventBuilder().PrintFreq = 1000 

ToA  = 4096;
SPDR = ToA * 16384
TbEventBuilder().EventLength = 1 * SPDR #400 * 4096 # 10000 ns = 10 us = 400*4096
TbEventBuilder().CacheLength = 2 * SPDR
# This is for the zero-suppression.
# If you look for the info in between spills comment this.
TbEventBuilder().MinPlanesWithHits = 3  #zero suppresion
TbEventBuilder().EndOnEOF = False
TbEventBuilder().ForceCaching = False
TbEventBuilder().Monitoring = True 
TbEventBuilder().PrintFreq  = 100
TbEventBuilder().MaxHitsPerEvent = 10e6
TbEventBuilder().LinkSynchronisation = True
TbEventBuilder().MaxEmpty = 10000
#TbEventBuilder().StartTime = 36.0 #s
#TbEventBuilder().EndTime = 36100 #ms 

from Configurables import TbHitMonitor
#TbHitMonitor().ParametersHitsInEvent = ("", 0., 1.e6, 1000)
TbHitMonitor().ParametersTimestamp = ("", 0., 7.e10, 100000)

from Configurables import TbClustering
TbClustering().PrintConfiguration = True
TbClustering().ClusterErrorMethod = 2 #default
TbClustering().ClusterError = (0.01, 0.01)
TbClustering().TimeWindow = 100 #default 100 ns

from Configurables import TbSimpleTracking
TbSimpleTracking().PrintConfiguration = True
#TbSimpleTracking().MaskedPlanes = [2]
TbSimpleTracking().MaskedPlanes = []
#TbSimpleTracking().MinPlanes = 3
TbSimpleTracking().MinPlanes = 4
TbSimpleTracking().TimeWindow = 60.
TbSimpleTracking().RemoveOutliers = False
TbSimpleTracking().Monitoring = False
TbSimpleTracking().RecheckTrack = True
TbSimpleTracking().MaxClusterWidth = 4
TbSimpleTracking().MaxChi2 = 10
TbSimpleTracking().MaxOpeningAngle = 0.1

from Configurables import TbTrackPlots
TbTrackPlots().ParametersSlope = ("", -0.01, 0.01, 100)
TbTrackPlots().RefitTracks = True
#TbTrackPlots().ParametersTime = ("", 0., 7.e10, 100000)
 
from Configurables import TbClusterPlots
TbClusterPlots().FillComparisonPlots = True
TbClusterPlots().ReferencePlane = 2 
TbClusterPlots().TimeWindow = 2. 
TbClusterPlots().FillTimeSlicesPlots = True
TbClusterPlots().SlicePlane = 3
TbClusterPlots().NSlices = 10 #default 100 
#TbClusterPlots().ParametersTime = ("", 0., 7.e10, 100000)

from Configurables import TbFlEfficiency
TbFlEfficiency().TimeWindow = 60. #ns, default 100ns
TbFlEfficiency().XWindow = 0.1 #mm, default 0.15 
TbFlEfficiency().MaxChi2 = 10
TbFlEfficiency().FiducialMinX = 1.6  
TbFlEfficiency().FiducialMaxX = 13.3
TbFlEfficiency().FiducialMinY = -12.5
TbFlEfficiency().FiducialMaxY = 0.5
#TbFlEfficiency().OutputLevel = DEBUG
#TbFlEfficiency().nTotalTracks = 150000
#Kepler().UserAlgorithms = [TbFlEfficiency()]

Kepler().WriteTuples = False
from Configurables import TbTupleWriter
TbTupleWriter().WriteHits   = False
TbTupleWriter().WriteClusters   = True
TbTupleWriter().WriteTracks = True
TbTupleWriter().WriteDUTOnly = False
TbTupleWriter().MaxClusterSize = 100 #default 200, single 300
#TbTupleWriter().DUT = 4 
#TbTupleWriter().TimeWindow = 30
TbTupleWriter().MaxChi = 10 #4


################### alignment ##############################
# Kepler().Alignment = False
# #from Configurables import TbAlignmentMinuit1
# #Kepler().addAlignment(TbAlignmentMinuit1(MaxChi2 = 100000., ReferencePlane = 2, MaskedPlanes = [3]))
# #from Configurables import TbAlignmentMinuit0
# #Kepler().addAlignment(TbAlignmentMinuit0(MaxChi2 = 10., ReferencePlane = 2, MaskedPlanes = [3]))
# from Configurables import TbMillepede
# Kepler().addAlignment(TbMillepede(MaskedPlanes = [3], MaxChi2 = 20., ResCut = 0.2, ResCutInit = 0.2)) 
# #Kepler().addAlignment(TbMillepede(MaskedPlanes = [3], MaxChi2 = 10., ResCut = 0.2, ResCutInit = 0.2)) 
# from Configurables import TbAlignment
# TbAlignment().NTracks = 1000000


def patch():
  from Configurables import TbHitMonitor, TbClusterPlots
  from Configurables import TbEventBuilder, TbClustering, TbTrackPlots
  #TbClusterPlots().FillComparisonPlots = True
  #GaudiSequencer("Telescope").Members = [TbEventBuilder() ,TbClustering(),TbSimpleTracking()]
  GaudiSequencer("Telescope").Members = [TbEventBuilder()]
  #from Configurables import TbCalibration
  #TbCalibration().CheckSynchronisation = True
  #TbCalibration().SyncMethod = 0
  #TbCalibration().TWindow = 50000
#  GaudiSequencer("Monitoring").Members = [TbCalibration()]
#  GaudiSequencer("Monitoring").Members = [TbHitMonitor()] 
  GaudiSequencer("Monitoring").Members = [] 
  #GaudiSequencer("Monitoring").Members = [TbHitMonitor(),TbClusterPlots(),TbTrackPlots()]

appendPostConfigAction(patch)


