#ifndef TB_TDCPIXDECODER_H
#define TB_TDCPIXDECODER_H 1

// Tb/TbEvent
#include "Event/TbHit.h"

// Tb/TbKernel
#include "TbKernel/TbAlgorithm.h"
#include "TbKernel/TbModule.h"

// Local
#include "TDCPixTimeStamp.hh"
#include "TbRawFileGTK.h"

/** @class TbTdcPixDecoder TbTdcPixDecoder.h
 *
 */

class TbTdcPixDecoder : public TbAlgorithm {
 public:
  /// Standard constructor
  TbTdcPixDecoder(const std::string& name, ISvcLocator* pSvcLocator);
  /// Destructor
  virtual ~TbTdcPixDecoder();

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute() override;     ///< Algorithm execution
  virtual StatusCode finalize() override;    ///< Algorithm termination

 private:
  /// TES location of output hits.
  std::string m_hitLocation;
  /// Device name.
  std::string m_id;
  /// Device index (from alignment file/geometry service).
  unsigned int m_device = 999;

  /// Flag to run in standalone-mode (as opposed together with the telescope).
  bool m_standalone;
  bool m_monitoring;
  /// Print frequency
  unsigned int m_printFreq;
  /// Number of frames to skip at the beginning.
  unsigned int m_nSkip = 0;
  /// Count number of frames.
  unsigned int m_nFrames = 0;
  /// Count number of pixel hits read.
  uint64_t m_nHitsRead = 0;

  /// List of input files.
  std::vector<TbRawFileGTK*> m_files;
  /// Iterator to the input file currently being read.
  std::vector<TbRawFileGTK*>::iterator m_currentFile;
  /// Current position in the input file.
  uint64_t m_pos = 0;

  /// Time offset (in global time units).
  int64_t m_offset = 0;
 
  /// Read one frame from the input file.
  bool readFrame(std::vector<GTK::TDCPixTimeStamp>& timeStamps,
                 unsigned int& frameSize, unsigned int& frameCount);
  /// Decode the start/end marker word of a frame.
  bool decodeFrameMarker(const std::array<unsigned char, 6>& buf,
                         unsigned int& frameCount) const;
  /// Make TbHits from time stamps and add them to the container.
  void createHits(const std::vector<GTK::TDCPixTimeStamp>& timeStamps, 
                  const unsigned int frameCount,
                  LHCb::TbHits* hits);
  /// Check if all files have been processed. Move to the next file if needed.
  bool eos();
  /// Convert the timestamp (in ns) to the global time units.
  uint64_t globalTimeStamp(const double ns) const {
    // Conversion factor (25 ns correspond to 4096 global time units).
    constexpr double f = 4096. / 25.;
    return floor(ns * f) + m_offset;
  }
  /// Print out the number of frames and hits read.
  void printCounters(const uint64_t nNewHits) const {
    info() << format(" %8u frames read, %12u hits", 
                     m_nFrames, m_nHitsRead + nNewHits) << endmsg;
  }
};

#endif
