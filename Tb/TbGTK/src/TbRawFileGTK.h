#pragma once

#include <array>

// Tb/TbKernel
#include "TbKernel/TbBufferedFile.h"

/** @class TbRawFileGTK TbRawFileGTK.h
 *
 * Interface for GTK raw files via TbBufferedFile (ROOT I/O)
 *
 */

class TbRawFileGTK : public TbBufferedFile<1000000, uint32_t> {
 public:
  /// Constructor
  TbRawFileGTK(const std::string& filename /*, TbHeaderDecoder* headerDecoder*/);
  /// Destructor
  virtual ~TbRawFileGTK() {}

  /// Return whether the file has been opened successfully
  bool good() const { return m_good; }
  void seek(const uint64_t pos) { m_file->Seek(pos); }
  /// Read buffer.
  void read(char* buf, const int len) {
    m_file->ReadBuffer(buf, len);
  }
  void readWord(std::array<unsigned char, 6>& bufEven,
                std::array<unsigned char, 6>& bufOdd) {
    std::array<char, 12> buffer;
    m_file->ReadBuffer(buffer.data(), buffer.size());
    bufEven[5] = buffer[3];
    bufEven[4] = buffer[2];
    bufEven[3] = buffer[1];
    bufEven[2] = buffer[0];
    bufEven[1] = buffer[7];
    bufEven[0] = buffer[6];

    bufOdd[5] = buffer[5];
    bufOdd[4] = buffer[4];
    bufOdd[3] = buffer[11];
    bufOdd[2] = buffer[10];
    bufOdd[1] = buffer[9];
    bufOdd[0] = buffer[8];
  }

 protected:
  bool m_good = true;
};
