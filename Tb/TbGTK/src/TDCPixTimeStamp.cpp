#include "TDCPixTimeStamp.hh"

#include <string.h>

namespace {

unsigned int binary_to_gray(const unsigned int num) { 
  return (num >> 1) ^ num; 
}

unsigned int gray_to_binary(unsigned int num) {
  const unsigned int numBits = 8 * sizeof(num);
  unsigned int shift = 0;
  for (shift = 1; shift < numBits; shift = 2 * shift) {
    num = num ^ (num >> shift);
  }
  return num;
}

unsigned int ones_count(const unsigned int num, const unsigned int nbits) {
  unsigned int count = 0;
  for (unsigned int i(0); i != nbits; i++) {
    unsigned int bit = 1 << i;
    if (bit & num) count++;
  }
  return count;
}

unsigned int parity(const unsigned int num, const unsigned int nbits) {
  return (ones_count(num, nbits) & 1);
}

int ConvertTimeStampIndices(unsigned int qchip,
                            unsigned int pixel_group_address,
                            unsigned int hit_arbiter_address,
                            unsigned int &column, unsigned int &pixel) {
  if (qchip == 0)
    qchip = 3;
  else if (qchip == 3)
    qchip = 0;
  else if (qchip == 1)
    qchip = 2;
  else if (qchip == 2)
    qchip = 1;

  column = qchip * 10 + (9 - pixel_group_address / 9);
  unsigned int hit_arbiter_idx = 0;
  switch (hit_arbiter_address) {
    case 1:
      hit_arbiter_idx = 0;
      break;
    case 2:
      hit_arbiter_idx = 1;
      break;
    case 4:
      hit_arbiter_idx = 2;
      break;
    case 8:
      hit_arbiter_idx = 3;
      break;
    case 16:
      hit_arbiter_idx = 4;
      break;
    default:
      return -1;
  };

  unsigned int r_pixel = pixel_group_address % 9 + hit_arbiter_idx * 9;
  // pixel=45-r_pixel-1;
  pixel = r_pixel;
  return 0;
}

}

namespace GTK {

TDCPixTimeStamp::TDCPixTimeStamp(int serial_fragment) 
    : mIsSerialFragment(serial_fragment) {
  memset(mBuf, 0, TDCPIX_TS_BUFFER_SIZE);
}

TDCPixTimeStamp::~TDCPixTimeStamp() {}

int TDCPixTimeStamp::Clear() {
  mQChip = 0;
  mIsSerialFragment = 0;
  mFrameCount = 0;

  memset(mBuf, 0, TDCPIX_TS_BUFFER_SIZE);
  return 0;
}

// const double TDCPixTimeStamp::ClockPeriod = 24.951059536;
const double TDCPixTimeStamp::ClockPeriod = 25.;

std::pair<unsigned int, double> TDCPixTimeStamp::GetLeadingTime(
    const double offset, const bool fCorr) const {

  unsigned short ft = gray_to_binary(this->GetLeadingFineTime());
  unsigned short ct = this->GetLeadingCoarseTime();
  unsigned short ct_bin = gray_to_binary(ct);

  if (ft > 20) {  // && ft<30){// use disambiguity mechanism:
    int par = parity(ct, 12);
    int sel = this->GetLeadingCoarseTimeSelector();
    if (par != sel) {                 // parity doesn't match selector:
      ct_bin = (ct_bin - 1) & 0xfff;  // what to do it ct == 0 => ct_bin = 0xfff
    }
  }

  // basic calculation
  double time = double((ct_bin & 0x7ff)) * ClockPeriod / 8.0;  // ps/clk cycle
  time +=
      (double(ft) + 0.5) * ClockPeriod / 256.0;  //=97.65625/2.0;// bin centre:

  unsigned int fc = this->GetFrameCount();
  // Due to queueing some time stamps cannot be sent in the frame they belong
  // to and get sent only in the next frame, so the frame counter has to be
  // corrected.
  const unsigned int ff = (ct_bin & 0x800) >> 11;
  if (((fc & 0x1) != ff) && fCorr) {
    fc = fc - 1;
  }
  return std::make_pair(fc, time + offset);
}

double TDCPixTimeStamp::GetTrailingTime(bool fCorr) const {
  // First find the frame to which the hit leadind time belong
  unsigned short ft_lead = gray_to_binary(this->GetLeadingFineTime());
  unsigned short ct_lead = this->GetLeadingCoarseTime();
  unsigned short ct_lead_bin = gray_to_binary(ct_lead);
  unsigned int lead_corr(0);

  if (ft_lead > 20) {  // && ft<30){// use disambiguity mechanism:
    int par = parity(ct_lead, 12);
    int sel = this->GetLeadingCoarseTimeSelector();
    if (par != sel) {  // parity doesn't match selector:
      ct_lead_bin = (ct_lead_bin - 1) & 0xfff;
      lead_corr = 1;
    }
  }
  unsigned int fc = this->GetFrameCount();
  const unsigned int ff = (ct_lead_bin & 0x800) >> 11;
  if (((fc & 0x1) != ff) && fCorr == 1) fc = fc - 1;

  // The trailing coarse counter should counts from the begining of the
  // leading frame. If the trailing falls in the next frame then
  // the coarse time should count over 4095.
  // In particular if raw_cc = 0 and the disambiguation puts it to 4094
  // the trailing coarse time  should be 4094 + tot and not 0+tot.
  // So the following has to be done:
  ct_lead_bin = ct_lead_bin + lead_corr;

  // Second compute the coarse trailing time
  // reset to original value
  unsigned short raw_ct_lead_bin = gray_to_binary(ct_lead);

  unsigned short tot = this->GetTrailingCoarseTime();
  unsigned short ct_trail_bin = (tot + raw_ct_lead_bin) & 0xfff;
  unsigned short ct_trail = binary_to_gray(ct_trail_bin);
  unsigned short ft_trail = gray_to_binary(this->GetTrailingFineTime());

  ct_trail_bin = ct_lead_bin + tot;
  if (ft_trail > 20) {
    int par = parity(ct_trail, 12);
    int sel = this->GetTrailingCoarseTimeSelector();
    if (par != sel) {  // parity doesn't match selector:
      ct_trail_bin--;
    }
  }
  double time = double(ct_trail_bin) * ClockPeriod / 8.0;
  time += double((ft_trail) + 0.5) * ClockPeriod / 256.0;
  time += double(fc & 0xfffffffe) * ClockPeriod * 256.0;

  return time;
}

unsigned short TDCPixTimeStamp::IsPaddingWord() const {
  const static unsigned char padding[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
  int rv = strncmp(reinterpret_cast<const char *>(mBuf),
                   reinterpret_cast<const char *>(padding), 6);
  return rv ? 0 : 1;
}

unsigned short TDCPixTimeStamp::GetTrailingFineTime() const {
  /* 4:0 */
  return 0x1f & mBuf[0];
}

unsigned short TDCPixTimeStamp::GetTrailingCoarseTime() const {
  /* 10:5 */
  unsigned short temp0 = mBuf[1] & 0x7;
  temp0 <<= 3;
  unsigned short temp1 = (mBuf[0] >> 5) & 0x7;

  return temp0 | temp1;
}
unsigned short TDCPixTimeStamp::GetTrailingCoarseTimeSelector() const {
  /* bit 11 */
  return (0x8 & mBuf[1]) ? 1 : 0;
}

unsigned short TDCPixTimeStamp::GetLeadingFineTime() const {
  /* 16:12 */
  unsigned short temp0 = (mBuf[2] << 4) & 0x10;
  unsigned short temp1 = (mBuf[1] >> 4) & 0xf;
  return temp0 | temp1;
}
unsigned short TDCPixTimeStamp::GetLeadingCoarseTime() const {
  /* 28:17 */
  unsigned short temp0 = mBuf[3] & 0x1f;
  temp0 <<= 7;
  unsigned short temp1 = (mBuf[2] >> 1) & 0x7f;
  return temp1 | temp0;
}
unsigned short TDCPixTimeStamp::GetLeadingCoarseTimeSelector() const {
  /* bit 29 */
  return (mBuf[3] >> 5) & 1;
}

unsigned short TDCPixTimeStamp::GetHitAddress() const {
  /* bits 39:35 */
  return 0x1f & (mBuf[4] >> 3);
}
unsigned short TDCPixTimeStamp::GetPileUpAddress() const {
  /* bits 34:30 */
  unsigned short temp0 = mBuf[4] & 0x7;
  temp0 <<= 2;
  unsigned short temp1 = (mBuf[3] >> 6) & 0x3;
  return temp1 | temp0;
}

unsigned short TDCPixTimeStamp::GetPixelGroupAddress() const {
  /* bits 46:40 */
  return mBuf[5] & 0x7f;
}

unsigned int TDCPixTimeStamp::GetNaturalPixelIndex() const {
  unsigned short pga = GetPixelGroupAddress();
  unsigned short haa = GetHitAddress();
  unsigned short qchip = GetQChip();

  unsigned int column = 0;
  unsigned int pixel = 0;
  ConvertTimeStampIndices(qchip, pga, haa,
                          column, pixel);

  return pixel;
}
unsigned int TDCPixTimeStamp::GetNaturalColumnIndex() const {
  unsigned short pga = GetPixelGroupAddress();
  unsigned short haa = GetHitAddress();
  unsigned short qchip = GetQChip();

  unsigned int column = 0;
  unsigned int pixel = 0;
  ConvertTimeStampIndices(qchip, pga, haa,
                          column, pixel);

  return column;
}

unsigned char TDCPixTimeStamp::Get6bitWord(unsigned int idx) const {
  /* idx must be less than 8 */
  char ebuf[128];
  unsigned char temp = 0;
  unsigned char temp2 = 0;
  switch (idx) {
    case 0:
      temp = mBuf[0] & 0x3f;
      break;
    case 1:
      temp2 = mBuf[1] & 0xf;
      temp2 <<= 2;
      temp = mBuf[0] & 0xc0;
      temp >>= 6;
      temp = temp | temp2;
      break;
    case 2:
      temp2 = mBuf[2] & 0x3;
      temp2 <<= 4;
      temp = mBuf[1] & 0xf0;
      temp >>= 4;
      temp = temp | temp2;
      break;
    case 3:
      temp = (mBuf[2] >> 2) & 0x3f;
      break;
    case 4:
      temp = mBuf[3] & 0x3f;
      break;
    case 5:
      temp2 = mBuf[4] & 0xf;
      temp2 <<= 2;
      temp = mBuf[3] & 0xc0;
      temp >>= 6;
      temp = temp | temp2;
      break;
    case 6:
      temp2 = mBuf[5] & 0x3;
      temp2 <<= 4;
      temp = mBuf[4] & 0xf0;
      temp >>= 4;
      temp = temp | temp2;
      break;
    case 7:
      temp = (mBuf[5] >> 2) & 0x3f;
      break;
    default:
      snprintf(ebuf, 120, "%d", idx);
      std::cerr << "invalid index passed to function: " << ebuf << "\n";
      return 0xff;
      break;
  };

  return temp;
}

int TDCPixTimeStamp::SetBuffer(const unsigned char *buf) {
  memcpy(mBuf, buf, TDCPIX_TS_BUFFER_SIZE);
  return 0;
}

unsigned int TDCPixTimeStamp::GetFrameCount() const {
  if (IsTimeStamp()) {
    return mFrameCount;
  }
  /* bits 27:0 */
  // return reinterpret_cast<const unsigned int *>(mBuf)[0] & 0x0fffffff;
  const unsigned int fc = *(reinterpret_cast<const unsigned int *>(mBuf[0])) & 0x0fffffff;
  return fc; 
}

int TDCPixTimeStamp::GetPixelUID() const {
  // Chips in GTK are labelled as:
  //  [for Sensor Upstream]
  //      ┌─┬─┬─┬─┬─┐
  //      │5│6│7│8│9│
  // Jura ├─┼─┼─┼─┼─┤ Saleve
  //      │0│1│2│3│4│
  //      └─┴─┴─┴─┴─┘
  //
  unsigned int qchip = this->GetQChip();
  unsigned int pixel_group_address = this->GetPixelGroupAddress();
  unsigned int hit_arbiter_address = this->GetHitAddress();
  if (qchip == 0)
    qchip = 2;
  else if (qchip == 1)
    qchip = 0;
  else if (qchip == 2)
    qchip = 1;
  else if (qchip == 3)
    qchip = 3;
  unsigned int x = qchip * 10 + (9 - pixel_group_address / 9);
  unsigned int hit_arbiter_idx = 0;
  switch (hit_arbiter_address) {
    case 1:
      hit_arbiter_idx = 0;
      break;
    case 2:
      hit_arbiter_idx = 1;
      break;
    case 4:
      hit_arbiter_idx = 2;
      break;
    case 8:
      hit_arbiter_idx = 3;
      break;
    case 16:
      hit_arbiter_idx = 4;
      break;
    default:
      return -1;
  };
  unsigned int y = pixel_group_address % 9 + hit_arbiter_idx * 9;
  unsigned int chipid = this->GetChipId() - 1;
  if (chipid < 4) {
    x = 39 - x;
    y = 44 - y;
  }
  y = 44 - y;
  int UID = x + chipid % 5 * 40 + y * 200 + chipid / 5 * 200 * 45;
  return UID;
}

std::ostream &operator<<(std::ostream &os, const TDCPixTimeStamp &ts) {
  if (!ts.IsTimeStamp()) {
    os << " not a timestamp\n";
    return os;
  }
  // this is a real time stamp:
  os << " time stamp data = ";
  os << "0x";
  for (unsigned int i(0); i != TDCPIX_TS_BUFFER_SIZE; i++) {
    int temp = ts.mBuf[TDCPIX_TS_BUFFER_SIZE - i - 1];
    os.width(2);
    os.fill('0');
    os << std::hex << temp;
  }

  os << std::dec;

  os << "\n";
  os << "   qchip address         = " << ts.GetQChip() << "\n";
  os << "   pixel address         = " << ts.GetPixelGroupAddress() << "\n";
  os << "   hit address           = " << ts.GetHitAddress() << "\n";
  os << "   pile up address       = " << ts.GetPileUpAddress() << "\n";
  os << "   leading fine time     = " << ts.GetLeadingFineTime() << "\n";
  os << "   leading coarse time   = " << ts.GetLeadingCoarseTime()
     << "[g] -> " << gray_to_binary(ts.GetLeadingCoarseTime()) << "[b]\n";
  os << "   leading ambiguity bit = " << ts.GetLeadingCoarseTimeSelector()
     << "\n";
  os << "   leading ct parity     = " << parity(ts.GetLeadingCoarseTime(), 12)
     << "\n";
  os << "   trailing fine time    = " << ts.GetTrailingFineTime() << "\n";
  os << "   trailing coarse time  = " << ts.GetTrailingCoarseTime() << "\n";
  os << "   trailing ambiguity bit= " << ts.GetTrailingCoarseTimeSelector()
     << "\n";
  os << "     row    = " << ts.GetNaturalPixelIndex() << "\n";
  os << "     column = " << ts.GetNaturalColumnIndex() << "\n";
  return os;
}

std::ostream &operator<<(std::ostream &os,
                         const std::vector<TDCPixTimeStamp> &vts) {
  for (const auto& ts : vts) os << ts;
  return os;
}
}
