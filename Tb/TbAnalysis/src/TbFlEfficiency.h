#ifndef TB_EFFICIENCY_H
#define TB_EFFICIENCY_H 1

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

// Tb/TbKernel
#include "TbKernel/TbAlgorithm.h"
#include "TbKernel/ITbTrackFit.h"
// Tb/TbEvent
#include "Event/TbTrack.h"

/** @class TbFlEfficiency TbFlEfficiency.h
 *
 */

class TbFlEfficiency : public TbAlgorithm {
 public:
  /// Constructor
  TbFlEfficiency(const std::string& name, ISvcLocator* pSvcLocator);
  /// Destructor
  virtual ~TbFlEfficiency() {}

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute() override;     ///< Algorithm execution

 private:
  unsigned int m_nTotalTracks;
  unsigned int m_nTracksConsidered = 0;

  bool m_checkHitAlivePixel;
  unsigned int m_pointingResAllowance_deadPixels;
  bool m_takeDeadPixelsFromFile;
  bool m_refit;
  // TH2F* m_deadPixelMap;

  double m_maxChi2;
  double m_twindow;
  double m_xwindow;

  // Fiducial area (in global coordinates).
  double m_minX;
  double m_maxX;
  double m_minY;
  double m_maxY;

  void fillEfficiency(LHCb::TbTrack* track, 
                      const std::vector<const LHCb::TbClusters*>& clusters);
  void fillEfficiencyFromNodes(LHCb::TbTrack* track, 
                      const std::vector<const LHCb::TbClusters*>& clusters);
  bool passedThroughAlivePixel(const int col, const int row,
                               const unsigned int plane) const;
  void setupPlots();

  AIDA::IHistogram1D* m_hPerPlaneTotal = nullptr;
  AIDA::IHistogram1D* m_hPerPlanePass = nullptr;
  AIDA::IHistogram1D* m_hNClustersPerTrack = nullptr;

  std::vector<AIDA::IHistogram1D*> m_hColTotal;
  std::vector<AIDA::IHistogram1D*> m_hRowTotal;
  std::vector<AIDA::IHistogram1D*> m_hColPass;
  std::vector<AIDA::IHistogram1D*> m_hRowPass;

  std::vector<AIDA::IHistogram2D*> m_hColRowTotal;
  std::vector<AIDA::IHistogram2D*> m_hColRowPass;

  std::vector<AIDA::IHistogram2D*> m_hIntraTotal;
  std::vector<AIDA::IHistogram2D*> m_hIntraPass;
  std::vector<AIDA::IHistogram2D*> m_hIntraPassCls1;
  std::vector<AIDA::IHistogram2D*> m_hIntraPassCls2;
  std::vector<AIDA::IHistogram2D*> m_hIntraPassCls3;
  std::vector<AIDA::IHistogram2D*> m_hIntraPassCls4;
  std::vector<AIDA::IHistogram2D*> m_hIntra2x2Total;
  std::vector<AIDA::IHistogram2D*> m_hIntra2x2Pass;

  std::vector<AIDA::IHistogram1D*> m_hNClustersAssociated;
  std::vector<AIDA::IHistogram1D*> m_hClusterSizeTotal;
  std::vector<AIDA::IHistogram1D*> m_hClusterSizePass;
  std::vector<AIDA::IHistogram1D*> m_hClusterSizeNotPass;

  AIDA::IHistogram1D* m_hNClustersTotal = nullptr;
  AIDA::IHistogram1D* m_hNClustersTracked = nullptr;

  std::string m_trackLocation;
  std::string m_clusterLocation;
  /// Name of the track fit tool
  std::string m_trackFitTool;
  /// Track fit tool
  ITbTrackFit* m_trackFit = nullptr;
};
#endif
