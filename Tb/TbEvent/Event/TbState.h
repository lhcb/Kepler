/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations
  // Namespace for locations in TDS
  namespace TbStateLocation {
    inline const std::string Default = "Rec/Tb/TbStates";
  }

  /** @class TbState TbState.h
   *
   * 4D state vector with z position
   *
   * @author Panagiotis Tsopelas, Wouter Hulsbergen
   *
   */

  class TbState {
  public:
    /// Default constructor
    TbState() : m_parameters(), m_covariance(), m_z( 0. ), m_plane( 0 ) {}

    /// Constructor with arguments
    TbState( const Gaudi::Vector4& state, const Gaudi::SymMatrix4x4& cov, const double z, const unsigned int plane )
        : m_parameters( state ), m_covariance( cov ), m_z( z ), m_plane( plane ) {}

    /// Default Destructor
    virtual ~TbState() {}

    /// Fill the ASCII output stream
    virtual std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve the squared error on the x-position of the state
    double errX2() const;

    /// Retrieve the squared error on the y-position of the state
    double errY2() const;

    /// Retrieve the squared error on the x-slope of the state
    double errTx2() const;

    /// Retrieve the squared error on the y-slope of the state
    double errTy2() const;

    /// Clone the TbState (you then own the pointer)
    virtual TbState* clone() const;

    /// Retrieve the 3D-position vector (x,y,z) of the state
    Gaudi::XYZPoint position() const;

    /// Retrieve the x-position of the state
    double x() const;

    /// Retrieve the y-position of the state
    double y() const;

    /// Retrieve the slopes (dx/dz, dy/dz, 1.) of the state
    Gaudi::XYZVector slopes() const;

    /// Retrieve the Tx=dx/dz slope of the state
    double tx() const;

    /// Retrieve the Ty=dy/dz slope of the state
    double ty() const;

    /// Update the x-position of the state
    void setX( const double value );

    /// Update the y-position of the state
    void setY( const double value );

    /// Update the z-position of the state
    void setZ( const double value );

    /// Update the x-slope tx = dx/dz of the state
    void setTx( const double value );

    /// Update the y-slope ty = dy/dz of the state
    void setTy( const double value );

    /// Update the state vector
    void setState( double x, double y, double tx, double ty, double z );

    /// Update the covariance matrix
    void setCovariance( const Gaudi::SymMatrix4x4& value );

    /// Print to message stream
    MsgStream& fillStream( MsgStream& os ) const;

    /// Retrieve const  the parameter vector
    const Gaudi::Vector4& parameters() const;

    /// Retrieve  the parameter vector
    Gaudi::Vector4& parameters();

    /// Retrieve const  covariance matrix (indices 0 ... 3 for x, y, tx, ty)
    const Gaudi::SymMatrix4x4& covariance() const;

    /// Retrieve  covariance matrix (indices 0 ... 3 for x, y, tx, ty)
    Gaudi::SymMatrix4x4& covariance();

    /// Retrieve const  the z-position of the state
    double z() const;

    /// Retrieve const  the plane of the state
    unsigned int plane() const;

    /// Update  the plane of the state
    void setPlane( unsigned int value );

    friend std::ostream& operator<<( std::ostream& str, const TbState& obj ) { return obj.fillStream( str ); }

  protected:
  private:
    Gaudi::Vector4      m_parameters; ///< the parameter vector
    Gaudi::SymMatrix4x4 m_covariance; ///< covariance matrix (indices 0 ... 3 for x, y, tx, ty)
    double              m_z;          ///< the z-position of the state
    unsigned int        m_plane;      ///< the plane of the state

  }; // class TbState

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline std::ostream& LHCb::TbState::fillStream( std::ostream& s ) const {
  s << "{ "
    << "parameters :	" << m_parameters << std::endl
    << "covariance :	" << m_covariance << std::endl
    << "z :	" << (float)m_z << std::endl
    << "plane :	" << m_plane << std::endl
    << " }";
  return s;
}

inline const Gaudi::Vector4& LHCb::TbState::parameters() const { return m_parameters; }

inline Gaudi::Vector4& LHCb::TbState::parameters() { return m_parameters; }

inline const Gaudi::SymMatrix4x4& LHCb::TbState::covariance() const { return m_covariance; }

inline Gaudi::SymMatrix4x4& LHCb::TbState::covariance() { return m_covariance; }

inline double LHCb::TbState::z() const { return m_z; }

inline unsigned int LHCb::TbState::plane() const { return m_plane; }

inline void LHCb::TbState::setPlane( unsigned int value ) { m_plane = value; }

inline double LHCb::TbState::errX2() const { return m_covariance( 0, 0 ); }

inline double LHCb::TbState::errY2() const { return m_covariance( 1, 1 ); }

inline double LHCb::TbState::errTx2() const { return m_covariance( 2, 2 ); }

inline double LHCb::TbState::errTy2() const { return m_covariance( 3, 3 ); }

inline Gaudi::XYZPoint LHCb::TbState::position() const {

  return Gaudi::XYZPoint( m_parameters[0], m_parameters[1], m_z );
}

inline double LHCb::TbState::x() const { return m_parameters[0]; }

inline double LHCb::TbState::y() const { return m_parameters[1]; }

inline Gaudi::XYZVector LHCb::TbState::slopes() const {

  return Gaudi::XYZVector( m_parameters[2], m_parameters[3], 1. );
}

inline double LHCb::TbState::tx() const { return m_parameters[2]; }

inline double LHCb::TbState::ty() const { return m_parameters[3]; }

inline void LHCb::TbState::setX( const double value ) { m_parameters[0] = value; }

inline void LHCb::TbState::setY( const double value ) { m_parameters[1] = value; }

inline void LHCb::TbState::setZ( const double value ) { m_z = value; }

inline void LHCb::TbState::setTx( const double value ) { m_parameters[2] = value; }

inline void LHCb::TbState::setTy( const double value ) { m_parameters[3] = value; }

inline void LHCb::TbState::setCovariance( const Gaudi::SymMatrix4x4& value ) { m_covariance = value; }

inline MsgStream& LHCb::TbState::fillStream( MsgStream& os ) const {

  os << "{ "
     << "parameters: " << m_parameters << std::endl
     << "z:          " << m_z << std::endl
     << " }";
  return os;
}
