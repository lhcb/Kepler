/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_TbTrigger = 4513;

  // Namespace for locations in TDS
  namespace TbTriggerLocation {
    inline const std::string Default = "Raw/Tb/Triggers";
  }

  /** @class TbTrigger TbTrigger.h
   *
   * Trigger timestamp
   *
   * @author H. Schindler
   *
   */

  class TbTrigger : public KeyedObject<int> {
  public:
    /// typedef for KeyedContainer of TbTrigger
    typedef KeyedContainer<TbTrigger, Containers::HashMap> Container;

    /// Constructor with pixel packet
    TbTrigger( const uint64_t& time, unsigned int counter );

    /// Copy constructor
    TbTrigger( const LHCb::TbTrigger* other );

    /// Default Constructor
    TbTrigger() : m_counter( 0 ), m_time(), m_htime( 0.0 ), m_associated(), m_plane( 0 ) {}

    /// Default Destructor
    virtual ~TbTrigger() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  counter
    unsigned int counter() const;

    /// Update  counter
    void setCounter( unsigned int value );

    /// Retrieve const  global timestamp
    const uint64_t& time() const;

    /// Update  global timestamp
    void setTime( const uint64_t& value );

    /// Retrieve const  local timestamp in ns
    double htime() const;

    /// Update  local timestamp in ns
    void setHtime( double value );

    /// Retrieve const  flag whether the trigger is associated to a track
    bool associated() const;

    /// Update  flag whether the trigger is associated to a track
    void setAssociated( bool value );

    /// Retrieve const  Plane of the trigger, dummy for the alignment
    int plane() const;

    /// Update  Plane of the trigger, dummy for the alignment
    void setPlane( int value );

    friend std::ostream& operator<<( std::ostream& str, const TbTrigger& obj ) { return obj.fillStream( str ); }

  protected:
  private:
    unsigned int m_counter;    ///< counter
    uint64_t     m_time;       ///< global timestamp
    double       m_htime;      ///< local timestamp in ns
    bool         m_associated; ///< flag whether the trigger is associated to a track
    int          m_plane;      ///< Plane of the trigger, dummy for the alignment

  }; // class TbTrigger

  /// Definition of Keyed Container for TbTrigger
  typedef KeyedContainer<TbTrigger, Containers::HashMap> TbTriggers;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::TbTrigger::TbTrigger( const uint64_t& time, unsigned int counter ) : m_associated( false ) {

  m_plane   = 0;
  m_time    = time;
  m_counter = counter;
}

inline LHCb::TbTrigger::TbTrigger( const LHCb::TbTrigger* other ) {

  m_plane      = other->plane();
  m_time       = other->time();
  m_counter    = other->counter();
  m_htime      = other->htime();
  m_associated = other->associated();
}

inline const CLID& LHCb::TbTrigger::clID() const { return LHCb::TbTrigger::classID(); }

inline const CLID& LHCb::TbTrigger::classID() { return CLID_TbTrigger; }

inline std::ostream& LHCb::TbTrigger::fillStream( std::ostream& s ) const {
  char l_associated = ( m_associated ) ? 'T' : 'F';
  s << "{ "
    << "counter :	" << m_counter << std::endl
    << "time :	" << m_time << std::endl
    << "htime :	" << (float)m_htime << std::endl
    << "associated :	" << l_associated << std::endl
    << "plane :	" << m_plane << std::endl
    << " }";
  return s;
}

inline unsigned int LHCb::TbTrigger::counter() const { return m_counter; }

inline void LHCb::TbTrigger::setCounter( unsigned int value ) { m_counter = value; }

inline const uint64_t& LHCb::TbTrigger::time() const { return m_time; }

inline void LHCb::TbTrigger::setTime( const uint64_t& value ) { m_time = value; }

inline double LHCb::TbTrigger::htime() const { return m_htime; }

inline void LHCb::TbTrigger::setHtime( double value ) { m_htime = value; }

inline bool LHCb::TbTrigger::associated() const { return m_associated; }

inline void LHCb::TbTrigger::setAssociated( bool value ) { m_associated = value; }

inline int LHCb::TbTrigger::plane() const { return m_plane; }

inline void LHCb::TbTrigger::setPlane( int value ) { m_plane = value; }
