/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_TbHit = 4510;

  // Namespace for locations in TDS
  namespace TbHitLocation {
    inline const std::string Default = "Raw/Tb/Hits";
  }

  /** @class TbHit TbHit.h
   *
   * Single pixel hit
   *
   * @author T. Evans
   *
   */

  class TbHit : public KeyedObject<int> {
  public:
    /// typedef for KeyedContainer of TbHit
    typedef KeyedContainer<TbHit, Containers::HashMap> Container;

    /// Default constructor
    TbHit() : m_associated( false ) {}

    /// Copy Constructor
    TbHit( const LHCb::TbHit* other );

    /// Default Destructor
    virtual ~TbHit() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  row number
    unsigned int row() const;

    /// Update  row number
    void setRow( unsigned int value );

    /// Retrieve const  column number
    unsigned int col() const;

    /// Update  column number
    void setCol( unsigned int value );

    /// Retrieve const  sensor column number
    unsigned int scol() const;

    /// Update  sensor column number
    void setScol( unsigned int value );

    /// Retrieve const  global timestamp
    const uint64_t& time() const;

    /// Update  global timestamp
    void setTime( const uint64_t& value );

    /// Retrieve const  local timestamp in ns
    double htime() const;

    /// Update  local timestamp in ns
    void setHtime( double value );

    /// Retrieve const  address of the pixel on the chip
    unsigned int pixelAddress() const;

    /// Update  address of the pixel on the chip
    void setPixelAddress( unsigned int value );

    /// Retrieve const  time over threshold
    unsigned int ToT() const;

    /// Update  time over threshold
    void setToT( unsigned int value );

    /// Retrieve const  collected charge (in electrons)
    double charge() const;

    /// Update  collected charge (in electrons)
    void setCharge( double value );

    /// Retrieve const  complete data line
    const uint64_t& data() const;

    /// Update  complete data line
    void setData( const uint64_t& value );

    /// Retrieve const  index of the chip
    unsigned int device() const;

    /// Update  index of the chip
    void setDevice( unsigned int value );

    /// Retrieve const  flag for whether the hit is associated to a track
    bool associated() const;

    /// Update  flag for whether the hit is associated to a track
    void setAssociated( bool value );

    friend std::ostream& operator<<( std::ostream& str, const TbHit& obj ) { return obj.fillStream( str ); }

  protected:
  private:
    unsigned int m_row;          ///< row number
    unsigned int m_col;          ///< column number
    unsigned int m_scol;         ///< sensor column number
    uint64_t     m_time;         ///< global timestamp
    double       m_htime;        ///< local timestamp in ns
    unsigned int m_pixelAddress; ///< address of the pixel on the chip
    unsigned int m_ToT;          ///< time over threshold
    double       m_charge;       ///< collected charge (in electrons)
    uint64_t     m_data;         ///< complete data line
    unsigned int m_device;       ///< index of the chip
    bool         m_associated;   ///< flag for whether the hit is associated to a track

  }; // class TbHit

  /// Definition of Keyed Container for TbHit
  typedef KeyedContainer<TbHit, Containers::HashMap> TbHits;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::TbHit::TbHit( const LHCb::TbHit* other ) {

  m_row          = other->row();
  m_col          = other->col();
  m_scol         = other->scol();
  m_time         = other->time();
  m_htime        = other->htime();
  m_pixelAddress = other->pixelAddress();
  m_ToT          = other->ToT();
  m_data         = other->data();
  m_device       = other->device();
  m_associated   = other->associated();
}

inline const CLID& LHCb::TbHit::clID() const { return LHCb::TbHit::classID(); }

inline const CLID& LHCb::TbHit::classID() { return CLID_TbHit; }

inline std::ostream& LHCb::TbHit::fillStream( std::ostream& s ) const {
  char l_associated = ( m_associated ) ? 'T' : 'F';
  s << "{ "
    << "row :	" << m_row << std::endl
    << "col :	" << m_col << std::endl
    << "scol :	" << m_scol << std::endl
    << "time :	" << m_time << std::endl
    << "htime :	" << (float)m_htime << std::endl
    << "pixelAddress :	" << m_pixelAddress << std::endl
    << "ToT :	" << m_ToT << std::endl
    << "charge :	" << (float)m_charge << std::endl
    << "data :	" << m_data << std::endl
    << "device :	" << m_device << std::endl
    << "associated :	" << l_associated << std::endl
    << " }";
  return s;
}

inline unsigned int LHCb::TbHit::row() const { return m_row; }

inline void LHCb::TbHit::setRow( unsigned int value ) { m_row = value; }

inline unsigned int LHCb::TbHit::col() const { return m_col; }

inline void LHCb::TbHit::setCol( unsigned int value ) { m_col = value; }

inline unsigned int LHCb::TbHit::scol() const { return m_scol; }

inline void LHCb::TbHit::setScol( unsigned int value ) { m_scol = value; }

inline const uint64_t& LHCb::TbHit::time() const { return m_time; }

inline void LHCb::TbHit::setTime( const uint64_t& value ) { m_time = value; }

inline double LHCb::TbHit::htime() const { return m_htime; }

inline void LHCb::TbHit::setHtime( double value ) { m_htime = value; }

inline unsigned int LHCb::TbHit::pixelAddress() const { return m_pixelAddress; }

inline void LHCb::TbHit::setPixelAddress( unsigned int value ) { m_pixelAddress = value; }

inline unsigned int LHCb::TbHit::ToT() const { return m_ToT; }

inline void LHCb::TbHit::setToT( unsigned int value ) { m_ToT = value; }

inline double LHCb::TbHit::charge() const { return m_charge; }

inline void LHCb::TbHit::setCharge( double value ) { m_charge = value; }

inline const uint64_t& LHCb::TbHit::data() const { return m_data; }

inline void LHCb::TbHit::setData( const uint64_t& value ) { m_data = value; }

inline unsigned int LHCb::TbHit::device() const { return m_device; }

inline void LHCb::TbHit::setDevice( unsigned int value ) { m_device = value; }

inline bool LHCb::TbHit::associated() const { return m_associated; }

inline void LHCb::TbHit::setAssociated( bool value ) { m_associated = value; }
