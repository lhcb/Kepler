/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/TbHit.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRefVector.h"

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_TbCluster = 4511;

  // Namespace for locations in TDS
  namespace TbClusterLocation {
    inline const std::string Default = "Rec/Tb/Clusters";
  }

  /** @class TbCluster TbCluster.h
   *
   * Generic cluster object for testbeam analysis
   *
   * @author H. Schindler
   *
   */

  class TbCluster : public KeyedObject<int> {
  public:
    /// typedef for KeyedContainer of TbCluster
    typedef KeyedContainer<TbCluster, Containers::HashMap> Container;

    /// Default constructor
    TbCluster();

    /// Default Destructor
    virtual ~TbCluster() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// Clone the cluster without keeping the hits (you take ownership of the pointer)
    virtual TbCluster* clone();

    /// Return the cluster size
    unsigned int size() const;

    /// Set the cluster and its hits to be associated to a track
    void setAssociated( const bool tracked );

    /// Retrieve const  global x
    double x() const;

    /// Update  global x
    void setX( double value );

    /// Retrieve const  global y
    double y() const;

    /// Update  global y
    void setY( double value );

    /// Retrieve const  global x error
    double xErr() const;

    /// Update  global x error
    void setXErr( double value );

    /// Retrieve const  global y error
    double yErr() const;

    /// Update  global y error
    void setYErr( double value );

    /// Retrieve const  global z
    double z() const;

    /// Update  global z
    void setZ( double value );

    /// Retrieve const  local x
    double xloc() const;

    /// Update  local x
    void setXloc( double value );

    /// Retrieve const  local y
    double yloc() const;

    /// Update  local y
    void setYloc( double value );

    /// Retrieve const  weight in global x
    double wx() const;

    /// Update  weight in global x
    void setWx( double value );

    /// Retrieve const  weight in global y
    double wy() const;

    /// Update  weight in global y
    void setWy( double value );

    /// Retrieve const  time over threshold
    unsigned int ToT() const;

    /// Update  time over threshold
    void setToT( unsigned int value );

    /// Retrieve const  total charge (in electrons)
    double charge() const;

    /// Update  total charge (in electrons)
    void setCharge( double value );

    /// Retrieve const  index of the telescope plane
    unsigned int plane() const;

    /// Update  index of the telescope plane
    void setPlane( unsigned int value );

    /// Retrieve const  global timestamp
    const uint64_t& time() const;

    /// Update  global timestamp
    void setTime( const uint64_t& value );

    /// Retrieve const  local timestamp in ns
    double htime() const;

    /// Update  local timestamp in ns
    void setHtime( double value );

    /// Retrieve const  number of columns covered by the cluster
    unsigned int cols() const;

    /// Update  number of columns covered by the cluster
    void setCols( unsigned int value );

    /// Retrieve const  number of rows covered by the cluster
    unsigned int rows() const;

    /// Update  number of rows covered by the cluster
    void setRows( unsigned int value );

    /// Retrieve const  flag for whether the cluster is part of a track
    bool associated() const;

    /// Retrieve (const)  hits forming this cluster
    const SmartRefVector<LHCb::TbHit>& hits() const;

    /// Update  hits forming this cluster
    void setHits( const SmartRefVector<LHCb::TbHit>& value );

    /// Add to  hits forming this cluster
    void addToHits( const SmartRef<LHCb::TbHit>& value );

    /// Att to (pointer)  hits forming this cluster
    void addToHits( const LHCb::TbHit* value );

    /// Remove from  hits forming this cluster
    void removeFromHits( const SmartRef<LHCb::TbHit>& value );

    /// Clear  hits forming this cluster
    void clearHits();

  protected:
  private:
    double                      m_x;          ///< global x
    double                      m_y;          ///< global y
    double                      m_xErr;       ///< global x error
    double                      m_yErr;       ///< global y error
    double                      m_z;          ///< global z
    double                      m_xloc;       ///< local x
    double                      m_yloc;       ///< local y
    double                      m_wx;         ///< weight in global x
    double                      m_wy;         ///< weight in global y
    unsigned int                m_ToT;        ///< time over threshold
    double                      m_charge;     ///< total charge (in electrons)
    unsigned int                m_plane;      ///< index of the telescope plane
    uint64_t                    m_time;       ///< global timestamp
    double                      m_htime;      ///< local timestamp in ns
    unsigned int                m_cols;       ///< number of columns covered by the cluster
    unsigned int                m_rows;       ///< number of rows covered by the cluster
    bool                        m_associated; ///< flag for whether the cluster is part of a track
    SmartRefVector<LHCb::TbHit> m_hits;       ///< hits forming this cluster

  }; // class TbCluster

  /// Definition of Keyed Container for TbCluster
  typedef KeyedContainer<TbCluster, Containers::HashMap> TbClusters;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::TbCluster::TbCluster() : m_wx( 1. ), m_wy( 1. ), m_associated( false ) { m_hits.reserve( 10 ); }

inline const CLID& LHCb::TbCluster::clID() const { return LHCb::TbCluster::classID(); }

inline const CLID& LHCb::TbCluster::classID() { return CLID_TbCluster; }

inline double LHCb::TbCluster::x() const { return m_x; }

inline void LHCb::TbCluster::setX( double value ) { m_x = value; }

inline double LHCb::TbCluster::y() const { return m_y; }

inline void LHCb::TbCluster::setY( double value ) { m_y = value; }

inline double LHCb::TbCluster::xErr() const { return m_xErr; }

inline void LHCb::TbCluster::setXErr( double value ) { m_xErr = value; }

inline double LHCb::TbCluster::yErr() const { return m_yErr; }

inline void LHCb::TbCluster::setYErr( double value ) { m_yErr = value; }

inline double LHCb::TbCluster::z() const { return m_z; }

inline void LHCb::TbCluster::setZ( double value ) { m_z = value; }

inline double LHCb::TbCluster::xloc() const { return m_xloc; }

inline void LHCb::TbCluster::setXloc( double value ) { m_xloc = value; }

inline double LHCb::TbCluster::yloc() const { return m_yloc; }

inline void LHCb::TbCluster::setYloc( double value ) { m_yloc = value; }

inline double LHCb::TbCluster::wx() const { return m_wx; }

inline void LHCb::TbCluster::setWx( double value ) { m_wx = value; }

inline double LHCb::TbCluster::wy() const { return m_wy; }

inline void LHCb::TbCluster::setWy( double value ) { m_wy = value; }

inline unsigned int LHCb::TbCluster::ToT() const { return m_ToT; }

inline void LHCb::TbCluster::setToT( unsigned int value ) { m_ToT = value; }

inline double LHCb::TbCluster::charge() const { return m_charge; }

inline void LHCb::TbCluster::setCharge( double value ) { m_charge = value; }

inline unsigned int LHCb::TbCluster::plane() const { return m_plane; }

inline void LHCb::TbCluster::setPlane( unsigned int value ) { m_plane = value; }

inline const uint64_t& LHCb::TbCluster::time() const { return m_time; }

inline void LHCb::TbCluster::setTime( const uint64_t& value ) { m_time = value; }

inline double LHCb::TbCluster::htime() const { return m_htime; }

inline void LHCb::TbCluster::setHtime( double value ) { m_htime = value; }

inline unsigned int LHCb::TbCluster::cols() const { return m_cols; }

inline void LHCb::TbCluster::setCols( unsigned int value ) { m_cols = value; }

inline unsigned int LHCb::TbCluster::rows() const { return m_rows; }

inline void LHCb::TbCluster::setRows( unsigned int value ) { m_rows = value; }

inline bool LHCb::TbCluster::associated() const { return m_associated; }

inline const SmartRefVector<LHCb::TbHit>& LHCb::TbCluster::hits() const { return m_hits; }

inline void LHCb::TbCluster::setHits( const SmartRefVector<LHCb::TbHit>& value ) { m_hits = value; }

inline void LHCb::TbCluster::addToHits( const SmartRef<LHCb::TbHit>& value ) { m_hits.push_back( value ); }

inline void LHCb::TbCluster::addToHits( const LHCb::TbHit* value ) { m_hits.push_back( value ); }

inline void LHCb::TbCluster::removeFromHits( const SmartRef<LHCb::TbHit>& value ) {
  auto i = std::remove( m_hits.begin(), m_hits.end(), value );
  m_hits.erase( i, m_hits.end() );
}

inline void LHCb::TbCluster::clearHits() { m_hits.clear(); }

inline unsigned int LHCb::TbCluster::size() const { return m_hits.size(); }

inline void LHCb::TbCluster::setAssociated( const bool tracked ) {

  m_associated = tracked;
  for ( auto it = m_hits.begin(), end = m_hits.end(); it != end; ++it ) { ( *it )->setAssociated( tracked ); }
}
