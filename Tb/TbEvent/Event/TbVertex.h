/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/TbTrack.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRefVector.h"

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_TbVertex = 4515;

  // Namespace for locations in TDS
  namespace TbVertexLocation {
    inline const std::string Default = "Rec/Tb/TbVertices";
  }

  /** @class TbVertex TbVertex.h
   *
   * Reconstructed telescope vertex
   *
   * @author D. Saunders
   *
   */

  class TbVertex : public KeyedObject<int> {
  public:
    /// typedef for KeyedContainer of TbVertex
    typedef KeyedContainer<TbVertex, Containers::HashMap> Container;

    /// Default Constructor
    TbVertex() : m_x( 0.0 ), m_y( 0.0 ), m_z( 0.0 ), m_htime( 0.0 ), m_interactionPlane( 0 ) {}

    /// Default Destructor
    virtual ~TbVertex() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// Retrieve const  x position of the vertex
    double x() const;

    /// Update  x position of the vertex
    void setX( double value );

    /// Retrieve const  y position of the vertex
    double y() const;

    /// Update  y position of the vertex
    void setY( double value );

    /// Retrieve const  z position of the vertex
    double z() const;

    /// Update  z position of the vertex
    void setZ( double value );

    /// Retrieve const  htime position of the vertex
    double htime() const;

    /// Update  htime position of the vertex
    void setHtime( double value );

    /// Retrieve const  plane forming the vertex
    unsigned int interactionPlane() const;

    /// Update  plane forming the vertex
    void setInteractionPlane( unsigned int value );

    /// Retrieve (const)  tracks connecting at the vertex
    const SmartRefVector<LHCb::TbTrack>& tracks() const;

    /// Update  tracks connecting at the vertex
    void setTracks( const SmartRefVector<LHCb::TbTrack>& value );

    /// Add to  tracks connecting at the vertex
    void addToTracks( const SmartRef<LHCb::TbTrack>& value );

    /// Att to (pointer)  tracks connecting at the vertex
    void addToTracks( const LHCb::TbTrack* value );

    /// Remove from  tracks connecting at the vertex
    void removeFromTracks( const SmartRef<LHCb::TbTrack>& value );

    /// Clear  tracks connecting at the vertex
    void clearTracks();

  protected:
  private:
    double                        m_x;                ///< x position of the vertex
    double                        m_y;                ///< y position of the vertex
    double                        m_z;                ///< z position of the vertex
    double                        m_htime;            ///< htime position of the vertex
    unsigned int                  m_interactionPlane; ///< plane forming the vertex
    SmartRefVector<LHCb::TbTrack> m_tracks;           ///< tracks connecting at the vertex

  }; // class TbVertex

  /// Definition of Keyed Container for TbVertex
  typedef KeyedContainer<TbVertex, Containers::HashMap> TbVertices;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::TbVertex::clID() const { return LHCb::TbVertex::classID(); }

inline const CLID& LHCb::TbVertex::classID() { return CLID_TbVertex; }

inline double LHCb::TbVertex::x() const { return m_x; }

inline void LHCb::TbVertex::setX( double value ) { m_x = value; }

inline double LHCb::TbVertex::y() const { return m_y; }

inline void LHCb::TbVertex::setY( double value ) { m_y = value; }

inline double LHCb::TbVertex::z() const { return m_z; }

inline void LHCb::TbVertex::setZ( double value ) { m_z = value; }

inline double LHCb::TbVertex::htime() const { return m_htime; }

inline void LHCb::TbVertex::setHtime( double value ) { m_htime = value; }

inline unsigned int LHCb::TbVertex::interactionPlane() const { return m_interactionPlane; }

inline void LHCb::TbVertex::setInteractionPlane( unsigned int value ) { m_interactionPlane = value; }

inline const SmartRefVector<LHCb::TbTrack>& LHCb::TbVertex::tracks() const { return m_tracks; }

inline void LHCb::TbVertex::setTracks( const SmartRefVector<LHCb::TbTrack>& value ) { m_tracks = value; }

inline void LHCb::TbVertex::addToTracks( const SmartRef<LHCb::TbTrack>& value ) { m_tracks.push_back( value ); }

inline void LHCb::TbVertex::addToTracks( const LHCb::TbTrack* value ) { m_tracks.push_back( value ); }

inline void LHCb::TbVertex::removeFromTracks( const SmartRef<LHCb::TbTrack>& value ) {
  auto i = std::remove( m_tracks.begin(), m_tracks.end(), value );
  m_tracks.erase( i, m_tracks.end() );
}

inline void LHCb::TbVertex::clearTracks() { m_tracks.clear(); }
