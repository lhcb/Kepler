/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/TbCluster.h"
#include "Event/TbNode.h"
#include "Event/TbState.h"
#include "Event/TbTrigger.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/VectorMap.h"
#include <algorithm>
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_TbTrack = 4512;

  // Namespace for locations in TDS
  namespace TbTrackLocation {
    inline const std::string Default = "Rec/Tb/TbTracks";
  }

  /** @class TbTrack TbTrack.h
   *
   * Reconstructed telescope track
   *
   * @author H. Schindler
   *
   */

  class TbTrack : public KeyedObject<int> {
  public:
    /// typedef for KeyedContainer of TbTrack
    typedef KeyedContainer<TbTrack, Containers::HashMap> Container;

    /// Method used for fitting the track
    enum FitStatus {
      None         = 0, // Track was not fitted
      StraightLine = 1, // Straight-line fit
      Kalman       = 2  // Kalman filter
    };

    /// Default constructor
    TbTrack();

    /// Copy constructor
    TbTrack( const LHCb::TbTrack& track );

    /// Default Destructor
    virtual ~TbTrack() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// conversion of string to enum for type FitStatus
    static LHCb::TbTrack::FitStatus FitStatusToType( const std::string& aName );

    /// conversion to string for enum type FitStatus
    static const std::string& FitStatusToString( int aEnum );

    /// Clear the node vector
    void clearNodes();

    /// Retrieve the chi-squared of the track
    double chi2() const;

    /// Clone the track without keeping the clusters (you take ownership of the pointer)
    virtual TbTrack* clone();

    /// Add a node to the list
    void addToNodes( const LHCb::TbNode& node );

    /// Return the number of clusters forming this track
    unsigned int size() const;

    /// Flag the clusters in a track as associated / unassociated
    void setAssociated( const bool flag );

    /// Retrieve const  global timestamp
    const uint64_t& time() const;

    /// Update  global timestamp
    void setTime( const uint64_t& value );

    /// Retrieve const  local timestamp in ns
    double htime() const;

    /// Update  local timestamp in ns
    void setHtime( double value );

    /// Retrieve const  fit status
    const LHCb::TbTrack::FitStatus& fitStatus() const;

    /// Update  fit status
    void setFitStatus( const LHCb::TbTrack::FitStatus& value );

    /// Retrieve const  first state of the track
    const LHCb::TbState& firstState() const;

    /// Update  first state of the track
    void setFirstState( const LHCb::TbState& value );

    /// Retrieve const  chi-squared per degree of freedom of the track
    double chi2PerNdof() const;

    /// Update  chi-squared per degree of freedom of the track
    void setChi2PerNdof( double value );

    /// Retrieve const  number of degrees of freedom
    unsigned int ndof() const;

    /// Update  number of degrees of freedom
    void setNdof( unsigned int value );

    /// Retrieve const  container with all nodes
    const std::vector<LHCb::TbNode>& nodes() const;

    /// Retrieve const  flag whether this track forms part of a vertex
    bool vertexed() const;

    /// Update  flag whether this track forms part of a vertex
    void setVertexed( bool value );

    /// Retrieve const  flag whether this track forms the start of a vertex
    bool parentVertex() const;

    /// Update  flag whether this track forms the start of a vertex
    void setParentVertex( bool value );

    /// Retrieve (const)  clusters forming this track
    const SmartRefVector<LHCb::TbCluster>& clusters() const;

    /// Update  clusters forming this track
    void setClusters( const SmartRefVector<LHCb::TbCluster>& value );

    /// Add to  clusters forming this track
    void addToClusters( const SmartRef<LHCb::TbCluster>& value );

    /// Att to (pointer)  clusters forming this track
    void addToClusters( const LHCb::TbCluster* value );

    /// Remove from  clusters forming this track
    void removeFromClusters( const SmartRef<LHCb::TbCluster>& value );

    /// Clear  clusters forming this track
    void clearClusters();

    /// Retrieve (const)  triggers associated to this track
    const SmartRefVector<LHCb::TbTrigger>& triggers() const;

    /// Update  triggers associated to this track
    void setTriggers( const SmartRefVector<LHCb::TbTrigger>& value );

    /// Add to  triggers associated to this track
    void addToTriggers( const SmartRef<LHCb::TbTrigger>& value );

    /// Att to (pointer)  triggers associated to this track
    void addToTriggers( const LHCb::TbTrigger* value );

    /// Remove from  triggers associated to this track
    void removeFromTriggers( const SmartRef<LHCb::TbTrigger>& value );

    /// Clear  triggers associated to this track
    void clearTriggers();

    /// Retrieve (const)  clusters associated to this track
    const SmartRefVector<LHCb::TbCluster>& associatedClusters() const;

    /// Update  clusters associated to this track
    void setAssociatedClusters( const SmartRefVector<LHCb::TbCluster>& value );

    /// Add to  clusters associated to this track
    void addToAssociatedClusters( const SmartRef<LHCb::TbCluster>& value );

    /// Att to (pointer)  clusters associated to this track
    void addToAssociatedClusters( const LHCb::TbCluster* value );

    /// Remove from  clusters associated to this track
    void removeFromAssociatedClusters( const SmartRef<LHCb::TbCluster>& value );

    /// Clear  clusters associated to this track
    void clearAssociatedClusters();

  protected:
  private:
    uint64_t                        m_time;               ///< global timestamp
    double                          m_htime;              ///< local timestamp in ns
    LHCb::TbTrack::FitStatus        m_fitStatus;          ///< fit status
    LHCb::TbState                   m_firstState;         ///< first state of the track
    double                          m_chi2PerNdof;        ///< chi-squared per degree of freedom of the track
    unsigned int                    m_ndof;               ///< number of degrees of freedom
    std::vector<LHCb::TbNode>       m_nodes;              ///< container with all nodes
    bool                            m_vertexed;           ///< flag whether this track forms part of a vertex
    bool                            m_parentVertex;       ///< flag whether this track forms the start of a vertex
    SmartRefVector<LHCb::TbCluster> m_clusters;           ///< clusters forming this track
    SmartRefVector<LHCb::TbTrigger> m_triggers;           ///< triggers associated to this track
    SmartRefVector<LHCb::TbCluster> m_associatedClusters; ///< clusters associated to this track

    static const GaudiUtils::VectorMap<std::string, FitStatus>& s_FitStatusTypMap();

  }; // class TbTrack

  /// Definition of Keyed Container for TbTrack
  typedef KeyedContainer<TbTrack, Containers::HashMap> TbTracks;

  inline std::ostream& operator<<( std::ostream& s, LHCb::TbTrack::FitStatus e ) {
    switch ( e ) {
    case LHCb::TbTrack::None:
      return s << "None";
    case LHCb::TbTrack::StraightLine:
      return s << "StraightLine";
    case LHCb::TbTrack::Kalman:
      return s << "Kalman";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::TbTrack::FitStatus";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::TbTrack::TbTrack()
    : m_time( 0 )
    , m_htime( 0. )
    , m_firstState()
    , m_chi2PerNdof( 0. )
    , m_ndof( 0 )
    , m_nodes()
    , m_vertexed( false )
    , m_parentVertex( false ) {

  m_clusters.reserve( 10 );
  m_triggers.reserve( 10 );
  m_associatedClusters.reserve( 10 );
  m_nodes.reserve( 10 );
}

inline const CLID& LHCb::TbTrack::clID() const { return LHCb::TbTrack::classID(); }

inline const CLID& LHCb::TbTrack::classID() { return CLID_TbTrack; }

inline const GaudiUtils::VectorMap<std::string, LHCb::TbTrack::FitStatus>& LHCb::TbTrack::s_FitStatusTypMap() {
  static const GaudiUtils::VectorMap<std::string, FitStatus> m = {
      {"None", None}, {"StraightLine", StraightLine}, {"Kalman", Kalman}};
  return m;
}

inline LHCb::TbTrack::FitStatus LHCb::TbTrack::FitStatusToType( const std::string& aName ) {
  auto iter = s_FitStatusTypMap().find( aName );
  return iter != s_FitStatusTypMap().end() ? iter->second : None;
}

inline const std::string& LHCb::TbTrack::FitStatusToString( int aEnum ) {
  static const std::string s_None = "None";
  auto                     iter   = std::find_if( s_FitStatusTypMap().begin(), s_FitStatusTypMap().end(),
                            [&]( const std::pair<const std::string, FitStatus>& i ) { return i.second == aEnum; } );
  return iter != s_FitStatusTypMap().end() ? iter->first : s_None;
}

inline const uint64_t& LHCb::TbTrack::time() const { return m_time; }

inline void LHCb::TbTrack::setTime( const uint64_t& value ) { m_time = value; }

inline double LHCb::TbTrack::htime() const { return m_htime; }

inline void LHCb::TbTrack::setHtime( double value ) { m_htime = value; }

inline const LHCb::TbTrack::FitStatus& LHCb::TbTrack::fitStatus() const { return m_fitStatus; }

inline void LHCb::TbTrack::setFitStatus( const LHCb::TbTrack::FitStatus& value ) { m_fitStatus = value; }

inline const LHCb::TbState& LHCb::TbTrack::firstState() const { return m_firstState; }

inline void LHCb::TbTrack::setFirstState( const LHCb::TbState& value ) { m_firstState = value; }

inline double LHCb::TbTrack::chi2PerNdof() const { return m_chi2PerNdof; }

inline void LHCb::TbTrack::setChi2PerNdof( double value ) { m_chi2PerNdof = value; }

inline unsigned int LHCb::TbTrack::ndof() const { return m_ndof; }

inline void LHCb::TbTrack::setNdof( unsigned int value ) { m_ndof = value; }

inline const std::vector<LHCb::TbNode>& LHCb::TbTrack::nodes() const { return m_nodes; }

inline bool LHCb::TbTrack::vertexed() const { return m_vertexed; }

inline void LHCb::TbTrack::setVertexed( bool value ) { m_vertexed = value; }

inline bool LHCb::TbTrack::parentVertex() const { return m_parentVertex; }

inline void LHCb::TbTrack::setParentVertex( bool value ) { m_parentVertex = value; }

inline const SmartRefVector<LHCb::TbCluster>& LHCb::TbTrack::clusters() const { return m_clusters; }

inline void LHCb::TbTrack::setClusters( const SmartRefVector<LHCb::TbCluster>& value ) { m_clusters = value; }

inline void LHCb::TbTrack::addToClusters( const SmartRef<LHCb::TbCluster>& value ) { m_clusters.push_back( value ); }

inline void LHCb::TbTrack::addToClusters( const LHCb::TbCluster* value ) { m_clusters.push_back( value ); }

inline void LHCb::TbTrack::removeFromClusters( const SmartRef<LHCb::TbCluster>& value ) {
  auto i = std::remove( m_clusters.begin(), m_clusters.end(), value );
  m_clusters.erase( i, m_clusters.end() );
}

inline void LHCb::TbTrack::clearClusters() { m_clusters.clear(); }

inline const SmartRefVector<LHCb::TbTrigger>& LHCb::TbTrack::triggers() const { return m_triggers; }

inline void LHCb::TbTrack::setTriggers( const SmartRefVector<LHCb::TbTrigger>& value ) { m_triggers = value; }

inline void LHCb::TbTrack::addToTriggers( const SmartRef<LHCb::TbTrigger>& value ) { m_triggers.push_back( value ); }

inline void LHCb::TbTrack::addToTriggers( const LHCb::TbTrigger* value ) { m_triggers.push_back( value ); }

inline void LHCb::TbTrack::removeFromTriggers( const SmartRef<LHCb::TbTrigger>& value ) {
  auto i = std::remove( m_triggers.begin(), m_triggers.end(), value );
  m_triggers.erase( i, m_triggers.end() );
}

inline void LHCb::TbTrack::clearTriggers() { m_triggers.clear(); }

inline const SmartRefVector<LHCb::TbCluster>& LHCb::TbTrack::associatedClusters() const { return m_associatedClusters; }

inline void LHCb::TbTrack::setAssociatedClusters( const SmartRefVector<LHCb::TbCluster>& value ) {
  m_associatedClusters = value;
}

inline void LHCb::TbTrack::addToAssociatedClusters( const SmartRef<LHCb::TbCluster>& value ) {
  m_associatedClusters.push_back( value );
}

inline void LHCb::TbTrack::addToAssociatedClusters( const LHCb::TbCluster* value ) {
  m_associatedClusters.push_back( value );
}

inline void LHCb::TbTrack::removeFromAssociatedClusters( const SmartRef<LHCb::TbCluster>& value ) {
  auto i = std::remove( m_associatedClusters.begin(), m_associatedClusters.end(), value );
  m_associatedClusters.erase( i, m_associatedClusters.end() );
}

inline void LHCb::TbTrack::clearAssociatedClusters() { m_associatedClusters.clear(); }

inline double LHCb::TbTrack::chi2() const { return m_chi2PerNdof * m_ndof; }

inline unsigned int LHCb::TbTrack::size() const { return m_clusters.size(); }

inline void LHCb::TbTrack::setAssociated( const bool flag ) {

  for ( auto it = m_clusters.begin(), end = m_clusters.end(); it != end; ++it ) { ( *it )->setAssociated( flag ); }
}
