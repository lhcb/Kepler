/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// begin include files
#include <vector>
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/ObjectVector.h"
#include "GaudiKernel/KeyedContainer.h"
#include "Event/TbVertex.h"
#include "Event/TbTrigger.h"
#include "Event/TbTrack.h"
#include "Event/TbState.h"
#include "Event/TbHit.h"
#include "Event/TbCluster.h"
// end include files

namespace {
  struct TbEvent_Instantiations {
    // begin instantiations
    KeyedContainer<LHCb::TbCluster>         m_KeyedContainer_LHCb__TbCluster;
    KeyedContainer<LHCb::TbHit>             m_KeyedContainer_LHCb__TbHit;
    KeyedContainer<LHCb::TbTrack>           m_KeyedContainer_LHCb__TbTrack;
    KeyedContainer<LHCb::TbTrigger>         m_KeyedContainer_LHCb__TbTrigger;
    KeyedContainer<LHCb::TbVertex>          m_KeyedContainer_LHCb__TbVertex;
    ObjectVector<LHCb::TbCluster>           m_ObjectVector_LHCb__TbCluster;
    ObjectVector<LHCb::TbHit>               m_ObjectVector_LHCb__TbHit;
    ObjectVector<LHCb::TbTrack>             m_ObjectVector_LHCb__TbTrack;
    ObjectVector<LHCb::TbTrigger>           m_ObjectVector_LHCb__TbTrigger;
    ObjectVector<LHCb::TbVertex>            m_ObjectVector_LHCb__TbVertex;
    SmartRef<LHCb::TbCluster>               m_SmartRef_LHCb__TbCluster;
    SmartRef<LHCb::TbHit>                   m_SmartRef_LHCb__TbHit;
    SmartRef<LHCb::TbTrack>                 m_SmartRef_LHCb__TbTrack;
    SmartRef<LHCb::TbTrigger>               m_SmartRef_LHCb__TbTrigger;
    SmartRef<LHCb::TbVertex>                m_SmartRef_LHCb__TbVertex;
    SmartRefVector<LHCb::TbCluster>         m_SmartRefVector_LHCb__TbCluster;
    SmartRefVector<LHCb::TbHit>             m_SmartRefVector_LHCb__TbHit;
    SmartRefVector<LHCb::TbTrack>           m_SmartRefVector_LHCb__TbTrack;
    SmartRefVector<LHCb::TbTrigger>         m_SmartRefVector_LHCb__TbTrigger;
    SmartRefVector<LHCb::TbVertex>          m_SmartRefVector_LHCb__TbVertex;
    std::vector<LHCb::TbCluster*>           m_std_vector_LHCb__TbClusterp;
    std::vector<LHCb::TbHit*>               m_std_vector_LHCb__TbHitp;
    std::vector<LHCb::TbState*>             m_std_vector_LHCb__TbStatep;
    std::vector<LHCb::TbTrack*>             m_std_vector_LHCb__TbTrackp;
    std::vector<LHCb::TbTrigger*>           m_std_vector_LHCb__TbTriggerp;
    std::vector<LHCb::TbVertex*>            m_std_vector_LHCb__TbVertexp;
    std::vector<SmartRef<LHCb::TbCluster> > m_std_vector_SmartRef_LHCb__TbCluster;
    std::vector<SmartRef<LHCb::TbHit> >     m_std_vector_SmartRef_LHCb__TbHit;
    std::vector<SmartRef<LHCb::TbTrack> >   m_std_vector_SmartRef_LHCb__TbTrack;
    std::vector<SmartRef<LHCb::TbTrigger> > m_std_vector_SmartRef_LHCb__TbTrigger;
    std::vector<SmartRef<LHCb::TbVertex> >  m_std_vector_SmartRef_LHCb__TbVertex;
    // end instantiations
  };
}
