// Tb/TbEvent
#include "Event/TbCluster.h"
#include "Event/TbHit.h"
#include "Event/TbTrack.h"
#include "Event/TbTrigger.h"
// Gaudi
#include "GaudiKernel/PhysicalConstants.h"
// Local
#include "TbKernel/TbFunctors.h"
#include "TbTupleWriter.h"

DECLARE_COMPONENT( TbTupleWriter )

//=============================================================================
// Standard constructor
//=============================================================================
TbTupleWriter::TbTupleWriter( const std::string& name, ISvcLocator* pSvcLocator ) : TbAlgorithm( name, pSvcLocator ) {
  declareProperty( "WriteTriggers", m_writeTriggers = false );
  declareProperty( "WriteHits", m_writeHits = false );
  declareProperty( "WriteClusters", m_writeClusters = true );
  declareProperty( "ClustersTrackedOnly", m_writeClustersTrackedOnly = false );
  declareProperty( "WriteTracks", m_writeTracks = false );

  declareProperty( "WriteDUTOnly", m_writeDUTOnly = true );
  declareProperty( "DUT", m_DUT = 9999 );

  declareProperty( "MaxClusterSize", m_maxclustersize = 200 );
  declareProperty( "TimeWindow", m_twindow = 10. * Gaudi::Units::ns );
  declareProperty( "WriteClusterHits", m_writeClusterHits = true );

  declareProperty( "TriggerLocation", m_triggerLocation = LHCb::TbTriggerLocation::Default );
  declareProperty( "HitLocation", m_hitLocation = LHCb::TbHitLocation::Default );
  declareProperty( "ClusterLocation", m_clusterLocation = LHCb::TbClusterLocation::Default );
  declareProperty( "TrackLocation", m_trackLocation = LHCb::TbTrackLocation::Default );

  /// Stripped nTuples for low rate external users,
  /// only output tracks that have associated triggers
  declareProperty( "StrippedNTuple", m_strippedNTuple = false );
  declareProperty( "MaxTriggers", m_maxTriggers = 20 );
  // Switch off output during finalize.
  setProperty( "NTuplePrint", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Destructor
//=============================================================================
TbTupleWriter::~TbTupleWriter() {}

//=============================================================================
// Initialisation
//=============================================================================
StatusCode TbTupleWriter::initialize() {
  // Initialise the base class.
  StatusCode sc = TbAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  if ( m_writeTriggers ) bookTriggers();
  if ( m_writeHits ) bookHits();
  if ( m_writeTracks ) bookTracks();
  if ( m_writeClusters ) bookClusters();
  if ( ( m_writeHits || m_writeClusters ) && m_writeDUTOnly && m_DUT == 9999 ) {
    warning() << "WriteDUTOnly is True but the DUT is not set." << endmsg;
  }
  m_evtNo = 0;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TbTupleWriter::execute() {
  if ( m_writeTriggers ) fillTriggers();
  if ( m_writeHits ) fillHits();
  if ( m_writeTracks ) fillTracks();
  if ( m_writeClusters ) fillClusters();
  ++m_evtNo;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Book trigger tuple
//=============================================================================
void TbTupleWriter::bookTriggers() {
  NTupleFilePtr file1( ntupleSvc(), "/NTUPLES/FILE1" );
  NTuplePtr     nt( ntupleSvc(), "/NTUPLES/FILE1/TbTupleWriter/Trigger" );
  // Check if already booked.
  if ( nt ) return;
  nt = ntupleSvc()->book( "/NTUPLES/FILE1/TbTupleWriter/Trigger", CLID_ColumnWiseTuple, "nTuple of Triggers" );
  nt->addItem( "TgID", m_TgID ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "TgTime", m_TgTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "TgHTime", m_TgHTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "TgEvt", m_TgEvt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "TgPlane", m_TgPlane ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "TgCounter", m_TgCounter ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Book hit tuple
//=============================================================================
void TbTupleWriter::bookHits() {
  NTupleFilePtr file1( ntupleSvc(), "/NTUPLES/FILE1" );
  NTuplePtr     nt( ntupleSvc(), "/NTUPLES/FILE1/TbTupleWriter/Hits" );
  // Check if already booked.
  if ( nt ) return;
  nt = ntupleSvc()->book( "/NTUPLES/FILE1/TbTupleWriter/Hits", CLID_ColumnWiseTuple, "nTuple of Hits" );
  nt->addItem( "hID", m_hID ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "hCol", m_hCol ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "hRow", m_hRow ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "hScol", m_hScol ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "hTime", m_hTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "hHTime", m_hHTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "hToT", m_hToT ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "hPlane", m_hPlane ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Book cluster tuple
//=============================================================================
void TbTupleWriter::bookClusters() {
  NTupleFilePtr file1( ntupleSvc(), "/NTUPLES/FILE1" );
  NTuplePtr     nt( ntupleSvc(), "/NTUPLES/FILE1/TbTupleWriter/Clusters" );
  // Check if already booked.
  if ( nt ) return;
  nt = ntupleSvc()->book( "/NTUPLES/FILE1/TbTupleWriter/Clusters", CLID_ColumnWiseTuple, "nTuple of Clusters" );
  nt->addItem( "clID", m_clID ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clGx", m_clGx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clGy", m_clGy ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clGz", m_clGz ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clLx", m_clLx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clLy", m_clLy ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clTime", m_clTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clHTime", m_clHTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clSize", m_clSize ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clColm", m_clColm ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clColM", m_clColM ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clSizeC", m_clSizeC ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clRowm", m_clRowm ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clRowM", m_clRowM ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clSizeR", m_clSizeR ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clCharge", m_clCharge ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clIsTracked", m_clTracked ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "trackGloX", m_xgloClu ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "trackGloY", m_ygloClu ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "trackLocX", m_xlocClu ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "trackLocY", m_ylocClu ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clTkTime", m_clTkTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clPlane", m_clPlane ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clEvtNo", m_clEvtNo ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clNHits", m_clN, 0, (int)m_maxclustersize ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addItem( "clEvtNHits", m_clEvtHits ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addIndexedItem( "hRow", m_clN, m_clhRow ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addIndexedItem( "hCol", m_clN, m_clhCol ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addIndexedItem( "sCol", m_clN, m_clsCol ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addIndexedItem( "hHTime", m_clN, m_clhHTime ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addIndexedItem( "hToT", m_clN, m_clhToT ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  nt->addIndexedItem( "hCharge", m_clN, m_clhCharge ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Book track tuple
//=============================================================================
void TbTupleWriter::bookTracks() {
  NTupleFilePtr file1( ntupleSvc(), "/NTUPLES/FILE1" );
  NTuplePtr     nt( ntupleSvc(), "/NTUPLES/FILE1/TbTupleWriter/Tracks" );
  // Check if already booked.
  if (nt) return;
  nt = ntupleSvc()->book("/NTUPLES/FILE1/TbTupleWriter/Tracks",
                         CLID_ColumnWiseTuple, "nTuple of Tracks");
  nt->addItem("NPlanes", m_NPlanes, 0, 10).ignore();
  nt->addItem("TkID", m_TkID).ignore();
  nt->addItem("TkTime", m_TkTime).ignore();
  nt->addItem("TkHTime", m_TkHTime).ignore();
  nt->addItem("TkNCl", m_TkNCl, 0, 10).ignore();
  nt->addItem("TkNAssoCl", m_TkNAssoCl, 0, 20).ignore();
  nt->addItem("TkX", m_TkX0).ignore();
  nt->addItem("TkY", m_TkY0).ignore();
  nt->addItem("TkTx", m_TkTx).ignore();
  nt->addItem("TkTy", m_TkTy).ignore();
  nt->addItem("TkChi2PerNdof", m_TkChi2ndof).ignore();
  nt->addIndexedItem("TkClId", m_NPlanes, m_TkClId).ignore();
  nt->addIndexedItem("TkClIdDuT", m_TkNAssoCl, m_TkClIdDuT).ignore();
  nt->addItem("TkEvt", m_TkEvt).ignore();
  nt->addItem("TkNTg", m_TkNTg, 0, (int)m_maxTriggers).ignore();
  nt->addIndexedItem("TkTgId", m_TkNTg, m_TkTgId).ignore();
  nt->addIndexedItem("TkTgPlane", m_TkNTg, m_TkTgPlane).ignore();
  nt->addIndexedItem("TkXResidual", m_NPlanes, m_TkXResidual).ignore();
  nt->addIndexedItem("TkYResidual", m_NPlanes, m_TkYResidual).ignore();
  nt->addIndexedItem("TkXResidualDuT", m_TkNAssoCl, m_TkXResidualDuT).ignore();
  nt->addIndexedItem("TkYResidualDuT", m_TkNAssoCl, m_TkYResidualDuT).ignore();
}

//=============================================================================
// Fill trigger tuple
//=============================================================================
void TbTupleWriter::fillTriggers() {
  const uint64_t evtOffset = (uint64_t)m_evtNo << 36;
  for ( unsigned int i = 0; i < m_nPlanes; ++i ) {
    const std::string       location = m_triggerLocation + std::to_string( i );
    const LHCb::TbTriggers* triggers = getIfExists<LHCb::TbTriggers>( location );
    if ( !triggers ) continue;
    for ( const LHCb::TbTrigger* trigger : *triggers ) {
      // if (m_triggerChannel != 999 && m_triggerChannel != trigger->plane())
      // continue;
      const uint64_t offset = ( (uint64_t)trigger->plane() << 32 ) + evtOffset;
      m_TgID                = trigger->index() + offset;
      m_TgTime              = trigger->time();
      m_TgHTime             = trigger->htime();
      m_TgPlane             = i;
      m_TgCounter           = trigger->counter();
      ntupleSvc()
          ->writeRecord( "/NTUPLES/FILE1/TbTupleWriter/Trigger" )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      m_TgEvt = m_evtNo;
    }
  }
}

//=============================================================================
// Fill hit tuple
//=============================================================================
void TbTupleWriter::fillHits() {
  const uint64_t evtOffset = (uint64_t)m_evtNo << 36;
  for ( unsigned int i = 0; i < m_nPlanes; ++i ) {
    if ( m_writeDUTOnly && i != m_DUT ) continue;
    const std::string   location = m_hitLocation + std::to_string( i );
    const LHCb::TbHits* hits     = getIfExists<LHCb::TbHits>( location );
    if ( !hits ) continue;
    const uint64_t offset = ( (uint64_t)i << 32 ) + evtOffset;
    for ( const LHCb::TbHit* hit : *hits ) {
      m_hID    = hit->index() + offset;
      m_hCol   = hit->col();
      m_hRow   = hit->row();
      m_hScol  = hit->scol();
      m_hTime  = hit->time();
      m_hHTime = hit->htime();
      m_hToT   = hit->ToT();
      m_hPlane = i;
      ntupleSvc()
          ->writeRecord( "/NTUPLES/FILE1/TbTupleWriter/Hits" )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }
}

//=============================================================================
// Fill cluster tuple
//=============================================================================
void TbTupleWriter::fillClusters() {
  const uint64_t        evtOffset = (uint64_t)m_evtNo << 36;
  const LHCb::TbTracks* tracks    = getIfExists<LHCb::TbTracks>( m_trackLocation );
  for ( unsigned int i = 0; i < m_nPlanes; ++i ) {
    if ( m_writeDUTOnly && i != m_DUT ) continue;
    unsigned long long      nHitsInEvt = 0;
    const std::string       location   = m_clusterLocation + std::to_string( i );
    const LHCb::TbClusters* clusters   = getIfExists<LHCb::TbClusters>( location );
    if ( !clusters ) continue;
    const uint64_t offset = ( (uint64_t)i << 32 ) + evtOffset;
    // Iterate over all clusters just to count number of hits in the event
    for ( const LHCb::TbCluster* cluster : *clusters ) { nHitsInEvt += cluster->hits().size(); }
    for ( const LHCb::TbCluster* cluster : *clusters ) {
      if ( m_writeClustersTrackedOnly && !cluster->associated() ) continue;
      const unsigned long long clid = cluster->index() + offset;
      m_clID                        = clid;
      m_clGx                        = cluster->x();
      m_clGy                        = cluster->y();
      m_clGz                        = cluster->z();
      m_clLx                        = cluster->xloc();
      m_clLy                        = cluster->yloc();
      m_clTime                      = cluster->time();
      m_clHTime                     = cluster->htime();
      m_clSize                      = cluster->hits().size();
      m_clCharge                    = cluster->charge();
      m_clTracked                   = cluster->associated();
      m_clPlane                     = cluster->plane();
      m_clEvtNo                     = m_evtNo;
      m_clN                         = m_clSize > m_maxclustersize ? m_maxclustersize : m_clSize;
      m_clEvtHits                   = nHitsInEvt;
      const auto& h0                = cluster->hits()[0];
      m_clColm                      = h0->scol();
      m_clColM                      = h0->scol();
      m_clRowm                      = h0->row();
      m_clRowM                      = h0->row();
      for ( unsigned int j = 0; j < m_clN; ++j ) {
        const auto& h  = cluster->hits()[j];
        m_clhRow[j]    = h->row();
        m_clhCol[j]    = h->col();
        m_clsCol[j]    = h->scol();
        m_clhToT[j]    = h->ToT();
        m_clhCharge[j] = h->charge();
        m_clhHTime[j]  = h->htime();
        if ( m_clColm > h->scol() ) m_clColm = h->scol();
        if ( m_clColM < h->scol() ) m_clColM = h->scol();
        if ( m_clRowm > h->row() ) m_clRowm = h->row();
        if ( m_clRowM < h->row() ) m_clRowM = h->row();
      }
      m_clSizeC = m_clColM - m_clColm + 1;
      m_clSizeR = m_clRowM - m_clRowm + 1;

      // Define the time window for the tracks
      const double tMin = cluster->htime() - m_twindow;
      const double tMax = cluster->htime() + m_twindow;

      // Loop over the tracks.
      if ( !tracks ) continue;
      for ( const LHCb::TbTrack* track : *tracks ) {

        // Stop when outside the time window.
        if ( track->htime() > tMax ) continue;
        if ( track->htime() < tMin ) continue;

        // Get global coordinates
        const Gaudi::XYZPoint pGlobal = geomSvc()->intercept( track, cluster->plane() );
        m_xgloClu                     = pGlobal.x();
        m_ygloClu                     = pGlobal.y();

        // Get local coordinates
        const auto pLocal = geomSvc()->globalToLocal( pGlobal, cluster->plane() );
        m_xlocClu         = pLocal.x();
        m_ylocClu         = pLocal.y();

        // Add the time of the track
        m_clTkTime = track->time();
      }

      ntupleSvc()
          ->writeRecord( "/NTUPLES/FILE1/TbTupleWriter/Clusters" )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }
}

//=============================================================================
// Fill track tuple
//=============================================================================
void TbTupleWriter::fillTracks() {
  const LHCb::TbTracks* tracks = getIfExists<LHCb::TbTracks>( m_trackLocation );
  if ( !tracks ) return;
  const uint64_t evtOffset = (uint64_t)m_evtNo << 36;
  for (const LHCb::TbTrack* track : *tracks) {
    if (m_strippedNTuple && track->triggers().empty()) continue;
    m_NPlanes = m_nPlanes;  
    for (unsigned int i = 0; i < m_nPlanes; ++i) { 
      m_TkClId[i] = 0;
      m_TkXResidual[i] = 0.;
      m_TkYResidual[i] = 0.;
    }
    m_TkX0 = track->firstState().x();
    m_TkY0 = track->firstState().y();
    m_TkTx = track->firstState().tx();
    m_TkTy = track->firstState().ty();
    m_TkHTime = track->htime();
    m_TkTime = track->time();
    m_TkID = track->index() + evtOffset;
    m_TkNCl = track->clusters().size();
    m_TkNAssoCl = track->associatedClusters().size();
    m_TkChi2ndof = track->chi2PerNdof();
    m_TkEvt = m_evtNo;
    const SmartRefVector<LHCb::TbCluster>& clusters = track->clusters();
    for (auto it = clusters.cbegin(), end = clusters.cend(); it != end; ++it) {
      const uint64_t offset = ((uint64_t)(*it)->plane() << 32) + evtOffset;
      const unsigned long long clid = (*it)->index() + offset;
      m_TkClId[(*it)->plane()] = clid;
      auto intercept = geomSvc()->intercept(track, (*it)->plane());
      m_TkXResidual[(*it)->plane()] = (*it)->x() - intercept.x();
      m_TkYResidual[(*it)->plane()] = (*it)->y() - intercept.y();
    }
    const SmartRefVector<LHCb::TbCluster>& associatedClusters =
        track->associatedClusters();
    unsigned int i = 0;
    for (auto it = associatedClusters.cbegin(), end = associatedClusters.cend();
         it != end; ++it) {
      const uint64_t offset = ((uint64_t)(*it)->plane() << 32) + evtOffset;
      const unsigned long long clid = (*it)->index() + offset;
      m_TkClIdDuT[i] = clid;
      auto intercept = geomSvc()->intercept(track, (*it)->plane());
      m_TkXResidualDuT[i] = (*it)->x() - intercept.x();
      m_TkYResidualDuT[i] = (*it)->y() - intercept.y();
      ++i;
    }
    m_TkNTg                                         = track->triggers().size();
    i                                               = 0;
    const SmartRefVector<LHCb::TbTrigger>& triggers = track->triggers();
    for ( auto it = triggers.cbegin(), end = triggers.cend(); it != end; ++it ) {
      const uint64_t           offset = ( ( uint64_t )( *it )->plane() << 32 ) + evtOffset;
      const unsigned long long clid   = ( *it )->index() + offset;
      if ( i == 20 ) {
        warning() << "More than 20 triggers associated to track: skipping" << endmsg;
        break;
      }
      m_TkTgId[i]    = clid;
      m_TkTgPlane[i] = ( *it )->plane();
      ++i;
    }
    ntupleSvc()
        ->writeRecord( "/NTUPLES/FILE1/TbTupleWriter/Tracks" )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
}
