#ifndef TBHEADERDECODER_H
#define TBHEADERDECODER_H 1

// Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "TbKernel/ITbGeometrySvc.h"
#include "TbKernel/ITbPixelSvc.h"

/** @class TbHeaderDecoder TbHeaderDecoder.h
 *
 * Tool to read SPIDR header from raw data file.
 *
 */

static const InterfaceID IID_TbHeaderDecoder("TbHeaderDecoder", 1, 0);

class TbHeaderDecoder : public GaudiTool {
 public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_TbHeaderDecoder; }

  /// Constructor
  TbHeaderDecoder(const std::string& type, const std::string& name,
                  const IInterface* parent);
  /// Destructor
  virtual ~TbHeaderDecoder();

  void print(const bool flag) { m_print = flag; }
  bool read(const char* data, unsigned int& deviceType, std::string& deviceId);

 private:
  /// Common part of the SPIDR header
  struct CommonHeader {
    uint32_t headerId;
    uint32_t headerSizeTotal;
    uint32_t headerSize;
    uint32_t format;
    uint32_t spidrId;
    uint32_t libVersion;
    uint32_t softwVersion;
    uint32_t firmwVersion;
    uint32_t ipAddress;
    uint32_t ipPort;
    uint32_t yyyyMmDd;
    uint32_t hhMmSsMs;
    uint32_t runNr;
    uint32_t seqNr;
    uint32_t spidrConfig;  // Trigger mode, decoder on/off
    uint32_t spidrFilter;
    uint32_t spidrBiasVoltage;
    // Spare
    uint32_t unused[128 - 17 - 128 / 4];
    // Description string
    char descr[128];
  };

  /// Flag to activate print-out or not.
  bool m_print = true;

  /// Pointer to geometry service
  mutable ITbGeometrySvc* m_geomSvc = nullptr;
  /// Access geometry service on-demand
  ITbGeometrySvc* geomSvc() const {
    if (!m_geomSvc) m_geomSvc = svc<ITbGeometrySvc>("TbGeometrySvc", true);
    return m_geomSvc;
  }

  /// Pointer to pixel service
  mutable ITbPixelSvc* m_pixelSvc = nullptr;
  /// Access pixel service on-demand
  ITbPixelSvc* pixelSvc() const {
    if (!m_pixelSvc) m_pixelSvc = svc<ITbPixelSvc>("TbPixelSvc", true);
    return m_pixelSvc;
  }
  /// Read Timepix3 header
  bool readTpx3(const char* data, std::string& deviceId);
  /// Read VeloPix header
  bool readVpx(const char* data, std::string& deviceId);
  /// Print information from the common part of the header
  void printCommonHeader(const CommonHeader& hdr);

};

#endif
